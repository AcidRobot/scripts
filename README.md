# scripts

## OpenCV installation
This will install OpenCV v2.4.10, and works on 
Hardware - Raspberry Pi 2 
OS - Raspbian Debian Wheezy

- **Step 1** - Update the raspberry pi
```shell
sudo apt-get update
sudo apt-get upgrade
sudo rpi-update
```
- **Step 2** - Remove pre-installed software
```shell
sudo apt-get -qq remove ffmpeg x264 libx264-dev
```
- **Step 3** - Install required Dependencies
```shell
sudo apt-get -qq install libusb-1.0-0.dev cmake pkg-config libjpeg8-dev libtiff4-dev libjasper-dev libpng12-dev libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libatlas-base-dev gfortran libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libqt4-dev libgtk2.0-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils ffmpeg cmake checkinstall
```
- **Step 4** - Get the installation && Unzip
```shell
wget -O opencv-2.4.10.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.10/opencv-2.4.10.zip/download
unzip opencv-2.4.10.zip
```
- **Step 5** - Make Build directory
```shell
cd opencv-2.4.10 && mkdir build && cd build
```
- **Step 6** - Build it
```shell
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_NEW_PYTHON_SUPPORT=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON  -D BUILD_EXAMPLES=ON ..
```
- **Step 7** - Make it [From what I remember this takes super long, definately longer than 2 hrs, do it with screen if you want to keep working(not recommended)]
```shell
sudo make
```
- **Step 8** - Install
```shell
sudo make install
```
- **Step 9** - Copy Library
```shell
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
```
- **Step 10** - Link Library
```shell
sudo ldconfig
```

##Disclaimer
I'm pretty sure this works with C && C++, just make sure to include the right libraries and link them when compiling.
I have only coded it in Python.
